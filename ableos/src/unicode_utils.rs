#![allow(dead_code)]

pub const CONSOLE: &str = "\u{E795}";
pub const POWER_SIGN: &str = "\u{23FB}";
pub const LAMBDA: &str = "λ";
