// TODO: Bitmasking
pub enum Mouse {
    Button1,
    Button2,
    Button3,
    Button4,
    Button5,
    X(i8),
    Y(i8),
    Wheel(i8),
}

pub trait PS2Mouse {
    fn movement();
    fn button();
}
