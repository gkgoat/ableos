use kernel::device_interface::character::CharacterDevice;

pub struct Serial {
    pub base: usize,
}

impl CharacterDevice for Serial {
    fn can_read(&self) -> bool {
        true
    }

    fn can_write(&self) -> bool {
        true
    }

    fn read_char(&mut self) -> Option<char> {
        todo!()
    }

    fn write_char(&mut self, _c: char) -> bool {
        todo!()
    }

    fn reset(&mut self) {
        todo!()
    }

    fn initialize(&mut self) -> bool {
        false
    }
}

pub fn new_serial_test() {
    let mut serial = Serial { base: 0x3F8 };
    serial.initialize();
    serial.write_char('a');
}
