/*
* Copyright (c) 2022, able <abl3theabove@gmail.com>
*
* SPDX-License-Identifier: MPL-2.0
*/

use core::panic::PanicInfo;
use log::error;

use crate::arch::interrupts::{bsod, BSODSource};
#[cfg(not(test))]
#[panic_handler]
fn panic_handler(info: &PanicInfo) -> ! {
    error!("{:?}", info);

    bsod(BSODSource::Panic(info));
}
