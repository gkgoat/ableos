use cpuio::{inb, inl, inw, outb, outl, outw};

pub fn read32(reg: u16) -> u32 {
    unsafe { inl(reg) }
}
pub fn read16(reg: u16) -> u16 {
    unsafe { inw(reg) as u16 }
}

pub fn read8(reg: u16) -> u8 {
    unsafe { inb(reg) }
}

pub fn write32(reg: u16, val: u32) {
    unsafe { outl(val, reg) }
}

pub fn write16(reg: u16, val: u16) {
    unsafe { outw(val, reg) }
}

pub fn write8(reg: u16, val: u8) {
    unsafe { outb(val, reg) }
}
