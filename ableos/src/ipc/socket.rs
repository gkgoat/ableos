// SEEALSO: https://git.ablecorp.us/able/ableos/src/branch/master/ableos/src/relib/network/socket.rs

use super::IPCError;

#[derive(Debug)]
pub struct Socket {
    pub public: bool,
    name: String,
    stream: Vec<u8>,
}

impl Socket {
    pub fn new<S: Into<String>>(name: S) -> Self {
        Self {
            public: false,
            name: name.into(),
            stream: vec![],
        }
    }

    // <S: Into<String>>(name: S)

    pub fn write<S: Into<Vec<u8>>>(&mut self, data: S) -> Result<(), IPCError> {
        for c in data.into() {
            self.stream.push(c as u8);
        }

        Ok(())
    }
    pub fn read(&mut self) -> Result<Vec<u8>, IPCError> {
        if self.stream.len() != 0 {
            let skt = self.stream.clone();
            self.stream = vec![];

            return Ok(skt);
        }
        return Err(IPCError::EmptySocket);
    }

    pub fn rename(&mut self, name: String) {
        self.name = name;
    }
}
