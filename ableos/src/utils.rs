//! Small generic utilities

#[inline]
pub fn type_of<T>(_: &T) -> &str {
    core::any::type_name::<T>()
}

#[no_mangle]
#[allow(unconditional_recursion)]
pub extern "C" fn stack_overflow() -> u8 {
    stack_overflow();
    // meme number
    69 // NOTE: Any specific reason for this number aside from memes?
}

#[cfg(test)]
mod tests {
    use super::*;

    // #[test]
    fn test_type_of() {
        assert_eq!(type_of(&1), "i32");
    }
}

pub const fn kb(x: usize) -> usize {
    x * 1024
}

pub const fn mb(x: usize) -> usize {
    x * 1024 * 1024
}

pub const fn gb(x: usize) -> usize {
    x * 1024 * 1024 * 1024
}
