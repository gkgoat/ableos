use core::ops::{BitAnd, BitOr, Shr};
pub type Rgba64 = u64;
pub enum ChannelValue {
    Dark,
    Low,
    Mid,
    High,
}
impl From<u8> for ChannelValue {
    fn from(b: u8) -> Self {
        use ChannelValue::*;
        match b {
            0x00..=0x3f => Dark,
            0x40..=0x7f => Low,
            0x80..=0xbf => Mid,
            0xc0..=0xff => High,
        }
    }
}
pub fn get_r(rgba: Rgba64) -> u8 {
    rgba.bitand(0xff_00_00_00).shr(0o30) as u8
}
pub fn get_g(rgba: Rgba64) -> u8 {
    rgba.bitand(0xff_00_00).shr(0o20) as u8
}
pub fn get_b(rgba: Rgba64) -> u8 {
    rgba.bitand(0xff_00).shr(0o10) as u8
}
pub fn get_a(rgba: Rgba64) -> u8 {
    (rgba & 0xff) as u8
}
pub fn set_r(rgba: Rgba64, r: u8) -> Rgba64 {
    rgba.bitand(0xffffffff_00_ff_ff_ff)
        .bitor((r as Rgba64).shr(0o30))
}
pub fn set_g(rgba: Rgba64, g: u8) -> Rgba64 {
    rgba.bitand(0xffffffff_ff_00_ff_ff)
        .bitor((g as Rgba64).shr(0o20))
}
pub fn set_b(rgba: Rgba64, b: u8) -> Rgba64 {
    rgba.bitand(0xffffffff_ff_ff_00_ff)
        .bitor((b as Rgba64).shr(0o10))
}
pub fn set_a(rgba: Rgba64, a: u8) -> Rgba64 {
    rgba.bitand(0xffffffff_ff_ff_ff_00).bitor(a as Rgba64)
}
pub fn rgba_div(a: Rgba64, b: Rgba64) -> Rgba64 {
    set_r(0, get_r(a) / get_r(b))
        | set_g(0, get_g(a) / get_g(b))
        | set_g(0, get_b(a) / get_b(b))
        | set_g(0, get_a(a) / get_a(b))
}
pub fn new_rgba64(r: u8, g: u8, b: u8, a: u8) -> Rgba64 {
    set_r(0, r) | set_g(0, g) | set_b(0, b) | set_a(0, a)
}
