pub fn sysinfo() {}
pub fn master() -> Option<Master> {
    Some(Master {
        brand_string: Some("riscv".to_string()),
    })
}

pub struct Master {
    // TODO: Rename struct
    // version_information: Option<VersionInformation>,
    // thermal_power_management_information: Option<ThermalPowerManagementInformation>,
    // structured_extended_information: Option<StructuredExtendedInformation>,
    // extended_processor_signature: Option<ExtendedProcessorSignature>,
    pub brand_string: Option<String>,
    // cache_line: Option<CacheLine>,
    // time_stamp_counter: Option<TimeStampCounter>,
    // physical_address_size: Option<PhysicalAddressSize>,
}
impl Master {
    pub fn brand_string(&self) -> Option<&str> {
        self.brand_string.as_deref()
    }
}
