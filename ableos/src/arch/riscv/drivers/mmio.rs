/// # Safety
///
/// We label the mmio function unsafe since
/// we will be working with raw memory. Rust cannot
/// make any guarantees when we do this.
#[inline(always)]
fn mmio_write(address: usize, offset: usize, value: u8) {
    // Set the pointer based off of the address
    let reg = address as *mut u8;

    // write_volatile is a member of the *mut raw
    // and we can use the .add() to give us another pointer
    // at an offset based on the original pointer's memory
    // address. NOTE: The add uses pointer arithmetic so it is
    // new_pointer = old_pointer + sizeof(pointer_type) * offset
    unsafe {
        reg.add(offset).write_volatile(value);
    }
}

/// # Safety
///
/// We label the mmio function unsafe since
/// we will be working with raw memory. Rust cannot
/// make any guarantees when we do this.
fn mmio_read(address: usize, offset: usize, value: u8) -> u8 {
    // Set the pointer based off of the address
    let reg = address as *mut u8;

    // read_volatile() is much like write_volatile() except it
    // will grab 8-bits from the pointer and give that value to us.
    // We don't add a semi-colon at the end here so that the value
    // is "returned".
    unsafe { reg.add(offset).read_volatile() }
}
