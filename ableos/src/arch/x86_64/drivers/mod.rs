pub mod allocator;
// pub mod graphics;
pub mod serial;
pub mod sysinfo;
pub mod timer;

#[deprecated(note = "The use of hardware specific drivers for VGA is discouraged")]
pub mod vga;
