/*
* Copyright (c) 2022, able <abl3theabove@gmail.com>
*
* SPDX-License-Identifier: MPL-2.0
*/

pub mod drivers;
pub mod gdt;
pub mod init;
pub mod interrupts;
pub mod memory;

use crate::arch::drivers::allocator;
use bootloader::{entry_point, BootInfo};
use x86_64::{instructions::hlt, VirtAddr};

#[cfg(not(test))]
entry_point![start];

#[cfg(not(test))]
#[no_mangle]
pub fn start(boot_info: &'static BootInfo) -> ! {
    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);

    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator =
        unsafe { memory::BootInfoFrameAllocator::init(&boot_info.memory_map) };

    // let page = Page::containing_address(VirtAddr::new(0xdeadbeaf000));
    // memory::create_example_mapping(page, &mut mapper, &mut frame_allocator);
    //
    // let page_ptr: *mut u64 = page.start_address().as_mut_ptr();
    // unsafe { page_ptr.offset(400).write_volatile(0xf021_f077_f065_804e) };

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    crate::kmain::kernel_main();
}

#[allow(unused)]
pub fn shutdown() -> ! {
    info!("Shutting down");
    unsafe {
        cpuio::outw(0x2000, 0x604);
    }

    sloop();
}

pub fn sloop() -> ! {
    loop {
        hlt();
    }
}

// TODO: Split up into the randomness and the password generation
pub fn generate_process_pass() -> u128 {
    // TODO: Move this into entropy_pool module
    use rdrand::RdRand;
    let gen = RdRand::new().unwrap();

    (gen.try_next_u64().unwrap() as u128) << 64 | (gen.try_next_u64().unwrap() as u128)
}
