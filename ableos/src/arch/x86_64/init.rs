// #![allow(clippy::print_literal)]
use super::{gdt, interrupts};
use crate::{logger, serial_println, TERM};

/// x86_64 initialization
pub fn init() {
    // use crate::{network::socket::SimpleSock, relib::network::socket::Socket};

    // let mut log_socket_id = SimpleSock::new();
    // log_socket_id.register_protocol("Logger".to_string());

    let result = logger::init();
    match result {
        Ok(_) => {
            info!("Logger initialized");
        }
        Err(err) => serial_println!("{}", err),
    }

    let mut term = TERM.lock();
    // term.initialize();
    term.set_dirty(true);
    term.draw_term();
    drop(term);

    gdt::init();

    interrupts::init_idt();
    unsafe { interrupts::PICS.lock().initialize() };
    x86_64::instructions::interrupts::enable();
}
