use crate::kmain::TICK;
use core::sync::atomic::Ordering;

#[cfg(target_arch = "x86_64")]
pub fn fetch_time() -> f64 {
    let time = x86_64::instructions::interrupts::without_interrupts(|| {
        TICK.load(Ordering::Relaxed) as f64
    });
    time
}

#[cfg(target_arch = "riscv64")]
pub fn fetch_time() -> f64 {
    let time = TICK.load(Ordering::Relaxed) as f64;
    time
}
