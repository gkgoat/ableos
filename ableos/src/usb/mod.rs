//!

pub enum UsbSpeed {
    /// Super-speed functions operate at up to 5 Gb/s.
    Super,

    /// High-speed functions operate at up to 480 Mb/s.
    High,

    /// Full-speed functions operate at up to 12 Mb/s.
    Full,
    
    /// Low-speed functions operate at up to 1.5 Mb/s.
    Low,
}
