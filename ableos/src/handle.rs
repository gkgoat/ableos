/*
 * Copyright (c) 2022, Umut İnan Erdoğan <umutinanerdogan@pm.me>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

//! A handle is a u128 with a set of permissions
//! and a resource connected to it

use crate::arch::generate_process_pass;
use core::fmt::Display;

#[derive(Debug)]
pub struct BinaryData {
    _name: String,
    _data: Vec<u8>,
}

#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
pub enum HandleResource {
    Channel,
    Socket,
    BinaryData,
    StorageDevice,
    FileDescriptor,
    FsNode,
}

#[derive(Debug, Eq, Hash, PartialEq, Clone, Copy)]
pub struct Handle {
    pub inner: u128,
    pub res: HandleResource,
}

impl Display for Handle {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Handle-{:032x}", self.inner)?;
        match &self.res {
            HandleResource::Channel => write!(f, "-Channel")?,
            HandleResource::BinaryData => write!(f, "-Binary")?,
            HandleResource::Socket => write!(f, "-Socket")?,
            HandleResource::StorageDevice => write!(f, "-StorageDevice")?,
            HandleResource::FileDescriptor => write!(f, "-FileDescriptor")?,
            HandleResource::FsNode => write!(f, "-FsNode")?,
        }

        Ok(())
    }
}

impl Handle {
    pub fn new(htype: HandleResource) -> Self {
        // FIXME: check if inner collides
        Self {
            inner: generate_process_pass(),
            res: htype,
        }
    }
}
