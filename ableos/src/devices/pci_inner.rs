//! map the DeviceClass via <https://pci-ids.ucw.cz/read/PD>
//!
//!

#[repr(C)]
pub enum Vendors {
    ThreeDfxInteractiveInc = 0x121a,
    ThreeDLabs = 0x3d3d,
    AllianceSemiconductorCorp = 0x1142,
    ARKLogicInc = 0xedd8,
    ATITechnologiesInc = 0x1002,
    AvanceLogicIncALI = 0x1005,
    ChipsandTechnologies = 0x102c,
    CirrusLogic = 0x1013,
    Compaq = 0x0e11,
    CyrixCorp = 0x1078,
    DiamondMultimediaSystems = 0x1092,
    DigitalEquipmentCorp = 0x1011,
    Iit = 0x1061,
    IntegratedMicroSolutionsInc = 0x10e0,
    IntelCorp = 0x8086,
    IntergraphicsSystems = 0x10ea,
    MacronixInc = 0x10d9,
    MatroxGraphicsInc = 0x102b,
    MiroComputersProductsAG = 0x1031,
    NationalSemiconductorCorp = 0x100b,
    NeoMagicCorp = 0x10c8,
    Number9ComputerCompany = 0x105d,
    NVidiaCorporation = 0x10de,
    NVidiaSgsthomson = 0x12d2,
    OakTechnologyInc = 0x104e,
    QuantumDesignsHKLtd = 0x1098,
    Real3D = 0x003d,
    Rendition = 0x1163,
    S3Inc = 0x5333,
    SierraSemiconductor = 0x10a8,
    SiliconIntegratedSystemsSiS = 0x1039,
    SiliconMotionInc = 0x126f,
    STBSystemsInc = 0x10b4,
    TexasInstruments = 0x104c,
    ToshibaAmericaInfoSystems = 0x1179,
    TridentMicrosystems = 0x1023,
    TsengLabsInc = 0x100c,
    TundraSemiconductorCorp = 0x10e3,
    VIATechnologiesInc = 0x1106,
    VMWareInc = 0x15ad,
    Weitek = 0x100e,
    Unknown = 0xffff,
}

pub struct PciDevice {
    pub vendor: Vendors,
}

#[allow(dead_code)]
fn scan_pci_bus() {}
