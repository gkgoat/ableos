pub mod dev_null;
pub mod dev_unicode;
pub mod dev_zero;

pub use kernel::device_interface::character::CharacterDevice;
