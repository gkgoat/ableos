use kernel::device_interface::character::CharacterDevice;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct DevNull;
impl CharacterDevice for DevNull {
    fn can_read(&self) -> bool {
        true
    }

    fn can_write(&self) -> bool {
        true
    }

    fn read_char(&mut self) -> Option<char> {
        Some(0x00 as char)
    }

    fn write_char(&mut self, _: char) -> bool {
        true
    }

    fn reset(&mut self) {
        todo!()
    }

    fn initialize(&mut self) -> bool {
        todo!()
    }
}
