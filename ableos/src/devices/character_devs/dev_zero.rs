use kernel::device_interface::character::CharacterDevice;

#[derive(Debug)]
pub struct DevZero;
impl CharacterDevice for DevZero {
    fn can_read(&self) -> bool {
        true
    }

    fn can_write(&self) -> bool {
        true
    }

    fn read_char(&mut self) -> Option<char> {
        Some(0 as char)
    }

    fn write_char(&mut self, _: char) -> bool {
        true
    }

    fn reset(&mut self) {}

    fn initialize(&mut self) -> bool {
        true
    }
}
