use kernel::device_interface::character::CharacterDevice;

#[derive(Debug)]
pub struct DevUnicode {
    pub next_write_char: char,
    pub next_read_char: char,
}

impl CharacterDevice for DevUnicode {
    fn can_read(&self) -> bool {
        true
    }

    fn can_write(&self) -> bool {
        true
    }

    fn read_char(&mut self) -> Option<char> {
        let c = self.next_read_char;
        self.next_read_char = add1_char(c);

        Some(c)
    }

    fn write_char(&mut self, c: char) -> bool {
        if self.next_write_char != c {
            return false;
        }

        true
    }

    fn reset(&mut self) {
        self.next_write_char = 0x00 as char;
        self.next_read_char = 0x00 as char;
    }

    fn initialize(&mut self) -> bool {
        true
    }
}

fn add1_char(c: char) -> char {
    if c == char::MAX {
        return 0x00 as char;
    }

    char::from_u32(c as u32 + 1).unwrap()
}
