#[derive(Debug)]
pub enum Vendor {
    Unknown = 0,
    Ati = 1002,
}

pub fn match_vendor(id: u16) -> Vendor {
    use Vendor::*;

    match id {
        1002 => Ati,
        _ => Unknown,
    }
}
