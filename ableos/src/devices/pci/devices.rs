/*
* Copyright (c) 2022, able <abl3theabove@gmail.com>
*
* SPDX-License-Identifier: MPL-2.0
*/

use super::vendors::Vendor::{self, *};
#[derive(PartialEq, Clone, Eq, Debug)]
pub struct DeviceID {
    pub vendor: Vendor,
    pub id: u16,
}
impl DeviceID {
    pub const fn new(vendor: Vendor, id: u16) -> Self {
        Self { vendor, id }
    }
}

pub const VMWARE_SVGA2: DeviceID = DeviceID::new(VMware, 0x0405);
pub const S3INC_TRIO64V2: DeviceID = DeviceID::new(S3Inc, 0x8900);
