/*
* Copyright (c) 2022, able <abl3theabove@gmail.com>
*
* SPDX-License-Identifier: MPL-2.0
*/

use core::fmt::Display;

#[derive(PartialEq, Debug, Clone, Eq)]
#[repr(u16)]
pub enum Vendor {
    Qemu = 0x1234,
    VMware = 0x15AD,
    VirtIO = 0x1AF4,
    Ati = 1002,
    Intel = 0x8086,
    S3Inc = 0x5333,
    Unknown(u16),
}

impl From<u16> for Vendor {
    fn from(vendor_id: u16) -> Self {
        use Vendor::*;
        match vendor_id {
            0x15AD => VMware,
            0x8086 => Intel,
            1002 => Ati,
            id => Unknown(id),
        }
    }
}

impl Into<u16> for Vendor {
    fn into(self) -> u16 {
        use Vendor::*;
        match self {
            VMware => 0x15AD,
            Ati => 1002,
            Qemu => 0x1234,
            VirtIO => 0x1AF4,
            Intel => 0x8086,
            S3Inc => 0x5333,
            Unknown(id) => id,
        }
    }
}

impl Display for Vendor {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        use Vendor::*;

        let mut ret: String = match self {
            Qemu => "\0PINK\0QEMU (0x1234)".into(),
            VirtIO => "\0PINK\0VirtIO (0x1AF4)".into(),
            VMware => "\0PINK\0VMWARE (0x15AD)".into(),
            S3Inc => "\0YELLOW\0S3 Incorporated (0x5333)".into(),
            Intel => "\0BLUE\0Intel Corp. (0x8086)".into(),
            Ati => "\0RED\0ATI (0x1002)".into(),

            Unknown(id) => format!("\0RED\0Unknown ({:#6})", id),
        };

        ret.push_str("\0RESET\0");

        write!(f, "{}", ret)?;
        Ok(())
    }
}
