/*
* Copyright (c) 2022, able <abl3theabove@gmail.com>
*
* SPDX-License-Identifier: MPL-2.0
*/

use super::devices::*;

pub fn check_pci_support(device_id: DeviceID) -> bool {
    match device_id {
        VMWARE_SVGA2 => true,
        S3INC_TRIO64V2 => true,
        _ => false,
    }
}
