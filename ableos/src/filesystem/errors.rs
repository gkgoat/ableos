/*
 * Copyright (c) 2022, Umut İnan Erdoğan <umutinanerdogan@pm.me>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#[derive(Copy, Clone, Debug)]
pub enum FsError {
    EndOfFile,
    InodeNotFound,
    InvalidDevice,
    InvalidPath,
    IsDirectory,
    NotAbsolute,
    NotADirectory,
    NotFound,
    Recursion,
    UnsupportedOperation,
}

impl Into<FsError> for ext2::error::Error {
    fn into(self) -> FsError {
        match self {
            ext2::error::Error::Other(_) => todo!(),
            ext2::error::Error::BadMagic { magic: _ } => todo!(),
            ext2::error::Error::OutOfBounds { index: _ } => todo!(),
            ext2::error::Error::AddressOutOfBounds {
                sector: _,
                offset: _,
                size: _,
            } => todo!(),
            ext2::error::Error::BadBlockGroupCount {
                by_blocks: _,
                by_inodes: _,
            } => todo!(),
            ext2::error::Error::InodeNotFound { inode: _ } => FsError::InodeNotFound,
            ext2::error::Error::NotADirectory { inode: _, name: _ } => FsError::NotADirectory,
            ext2::error::Error::NotAbsolute { name: _ } => todo!(),
            ext2::error::Error::NotFound { name: _ } => FsError::NotFound,
        }
    }
}
