/*
 * Copyright (c) 2022, Umut İnan Erdoğan <umutinanerdogan@pm.me>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

#![allow(clippy::empty_loop)]

use core::sync::atomic::AtomicU64;

use crate::arch::{drivers::sysinfo::master, init, sloop};
use crate::relib::network::socket::{SimpleSock, Socket};
use crate::{
    boot_conf::KernelConfig, scratchpad, systeminfo::RELEASE_TYPE, TERM,
};
use crate::{filesystem, hardware};
use kernel::KERNEL_VERSION;
use spin::Lazy;

// TODO: Change this structure to allow for multiple cores loaded
pub static KERNEL_CONF: Lazy<KernelConfig> = Lazy::new(KernelConfig::new);

/// The main entry point of the kernel
#[no_mangle]
pub fn kernel_main() -> ! {
    init::init();

    // /*
    if KERNEL_CONF.logging.enabled {
        log::set_max_level(KERNEL_CONF.log_level());
        // println!("{:#?}", *KERNEL_CONF);
    } else {
        log::set_max_level(log::LevelFilter::Off);
    }
    // */
    // let mut term = TERM.lock();
    // term.initialize();
    // term.set_dirty(true);
    // term.draw_term();
    // drop(term);

    x86_64::instructions::interrupts::without_interrupts(|| {
        hardware::init_mouse();
    });

    filesystem::init().unwrap();

    /*

    // println!("abc");
    let mut ipc_service = IPC.lock();
    // create some channels or whatever here then drop it
    let log_handle = ipc_service.create_channel("LOG", true);
    let log_sock_handle = ipc_service.create_socket("LOG", true);

    for handle in ipc_service.examine_board() {
        // println!("Discovered: {}", handle);
    }

    drop(ipc_service);


    */

    // use x86_ata::{init, ATA_BLOCK_SIZE};
    // 1. Initialise ATA Subsystem. (Perform Once, on boot)
    // init().expect("Failed To Start ATA...");
    // let mut buffer: [u8; ATA_BLOCK_SIZE] = [0; ATA_BLOCK_SIZE];
    // FIXME: Calls to read panic the kernel
    // read(0, 0, 0, &mut buffer);

    // for abc in list() {
    //     trace!("{:?}", abc);
    // }
    /*
    log_version_data();
    // */
    scratchpad();
    sloop()
}

pub fn traceloop() {
    // TODO: Having an empty function double faults

    // let mut last_time = 0.0;
    /*
    loop {
        // FIXME: the following double faults
        /*
        let time = fetch_time();
        if time > last_time {
            last_time = time;
            trace!("Timer");
        }
        */
    }

    */

    /* TODO: This also double faults
    let fs = &*crate::filesystem::FILE_SYSTEM.lock();
    let path = format!("/home/able/bins/aos_test.wasm");

    let home_exec_file = fs.open(&path.as_bytes(), OpenOptions::new().read(true));

    drop(fs);
    let mut binary_prog: Vec<u8> = vec![];

    match home_exec_file {
        Ok(file) => {
            let ret = file.read_to_end(&mut binary_prog).unwrap();
        }
        _ => {}
    }

    wasm_jumploader::run_program(&binary_prog);
    */
}

pub fn cpu_socket_startup() {
    let mut cpu_info_socket = SimpleSock::new();
    cpu_info_socket.register_protocol("CPU_INFO".to_string());

    // let x = master().unwrap();
    // let _xyz = x.brand_string().unwrap();
}

pub fn log_version_data() {
    info!("{} v{}", RELEASE_TYPE, KERNEL_VERSION);
    info!(
        "Brand String: {}",
        master().unwrap().brand_string().unwrap()
    );
}

pub static TICK: AtomicU64 = AtomicU64::new(0);

pub fn tick() {
    x86_64::instructions::interrupts::without_interrupts(|| {
        let mut term = TERM.lock();

        term.draw_term();
        use core::sync::atomic::Ordering::Relaxed;

        let mut data = TICK.load(Relaxed);
        data = data.wrapping_add(1);

        TICK.store(data, Relaxed);
    });
}
