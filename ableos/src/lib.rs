/*
 * Copyright (c) 2022, Able <able@ablecorp.us>
 * Copyright (c) 2022, Umut İnan Erdoğan <umutinanerdogan@pm.me>
 *
 * SPDX-License-Identifier: MPL-2.0
 */

//! main library for the AbleOS kernel.
//! exposing all the kernel functionality to the rest of the kernel.
//!
//!

#![no_std]
#![feature(
    abi_x86_interrupt,
    alloc_error_handler,
    asm_sym, // Needed for RISC-V
    naked_functions,
    prelude_import,
)]
#![feature(arbitrary_enum_discriminant)]
#![feature(custom_test_frameworks)]
#![reexport_test_harness_main = "test_main"]
#![test_runner(crate::test_runner)]

#[macro_use]
pub extern crate log;

#[macro_use]
pub extern crate bitflags;

pub extern crate alloc;
pub extern crate externc_libm as libm;

/// Contains architecture specific code for aarch64.
#[cfg(target_arch = "aarch64")]
#[path = "arch/aarch64/mod.rs"]
pub mod arch;

/// Contains architecture specific code for riscv64.
#[cfg(target_arch = "riscv64")]
#[path = "arch/riscv/mod.rs"]
pub mod arch;

/// Contains architecture specific code for x86_64.
#[cfg(target_arch = "x86_64")]
#[path = "arch/x86_64/mod.rs"]
pub mod arch;

#[cfg(target_arch = "x86_64")]
pub mod port_io;

#[macro_use]
pub mod print;

#[macro_use]
pub mod serial_print;

pub mod boot_conf;
pub mod devices;
pub mod driver_traits;
pub mod experiments;
pub mod filesystem;
pub mod graphics;
pub mod kernel_state;
pub mod keyboard;
pub mod kmain;
pub mod logger;
pub mod prelude;
pub mod ps2_mouse;
pub mod relib;
pub mod rhai_shell;
pub mod scratchpad;
pub mod stdio;
pub mod time;
pub mod utils;
pub mod virtio;
pub mod wasm;
pub mod wasm_jumploader;

pub mod allocator;

// pub use allocator as aalloc;
pub mod handle;
pub mod hardware;
pub mod ipc;
pub mod panic;
mod unicode_utils;
pub mod vga_e;
pub mod vterm;
// pub mod vgai;

#[prelude_import]
pub use prelude::rust_2021::*;

pub use driver_traits::*;
pub use experiments::*;
pub use graphics::*;
pub use kernel;
pub use kernel::messaging;
// pub use kernel::panic;
pub use kernel_state::*;
pub use keyboard::*;
pub use logger::*;
pub use relib::*;
pub use scratchpad::*;
pub use utils::*;
pub use virtio::*;
pub use wasm::*;

#[cfg(test)]
pub mod tests;

#[cfg(test)]
pub use tests::test_kernel_main;

#[cfg(test)]
use crate::tests::test_runner;
