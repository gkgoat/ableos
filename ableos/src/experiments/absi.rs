use logos::{Lexer, Logos};

// TODO improve tokenizer/parser
pub fn colorify(eval: &str) {
    let y = eval.split('$');
    for z in y {
        match z {
            "BLACK" => {
                // set_vga_color(Color::Black, Color::Black);
            }
            "RED" => {
                // set_vga_color(Color::Red, Color::Black);
            }
            "GREEN" => {
                // set_vga_color(Color::Green, Color::Black);
            }
            "BLUE" => {
                // set_vga_color(Color::Blue, Color::Black);
            }
            "CYAN" => {
                // set_vga_color(Color::Cyan, Color::Black);
            }
            "MAGENTA" => {
                // set_vga_color(Color::Magenta, Color::Black);
            }
            "BROWN" => {
                // set_vga_color(Color::Brown, Color::Black);
            }
            "LIGHTGRAY" => {
                // set_vga_color(Color::LightGray, Color::Black);
            }
            "DARKGRAY" => {
                // set_vga_color(Color::DarkGray, Color::Black);
            }
            "LIGHTBLUE" => {
                // set_vga_color(Color::LightBlue, Color::Black);
            }
            "LIGHTGREEN" => {
                // set_vga_color(Color::LightGreen, Color::Black);
            }
            "LIGHTCYAN" => {
                // set_vga_color(Color::LightCyan, Color::Black);
            }
            "LIGHTRED" => {
                // set_vga_color(Color::LightRed, Color::Black);
            }
            "PINK" => {
                // set_vga_color(Color::Pink, Color::Black);
            }
            "YELLOW" => {
                // set_vga_color(Color::Yellow, Color::Black);
            }
            "WHITE" => {
                // set_vga_color(Color::White, Color::Black);
            }
            "RESET" => {
                // set_vga_color(Color::White, Color::Black);
            }
            _elk => {
                // kprint!("{}", elk);
            }
        }
    }
}

#[derive(Logos, Debug, PartialEq)]
pub enum Token {
    // Hex(u32),
    #[regex(r"\$RED\$")]
    Red,

    #[regex(r"\$RESET\$")]
    Reset,

    #[regex("[a-zA-Z!@#$%^&*\">()\n ]+", parse_text, priority = 2)]
    Text(String),

    #[error]
    #[regex(r"[ \t\n\f]+", logos::skip)]
    Error,
}

pub fn colorify_2(eval: &str) {
    let lexer = Token::lexer(eval);
    for token in lexer {
        use Token::*;
        match token {
            Red => {
                // set_vga_color(Color::Red, Color::Black);
            }
            Reset => {
                // set_vga_color(Color::White, Color::Black);
            }
            Text(_text) => {
                // kprint!("{}", text);
            }
            err => {
                error!("Error parsing {:?}", err);
            }
        }
    }
}

fn parse_text(lex: &mut Lexer<Token>) -> Option<String> {
    let slice = lex.slice();
    Some(String::from(slice))
}
