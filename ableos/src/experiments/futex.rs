use core::time::Duration;

pub struct AtomicU32(u32);
impl AtomicU32 {
    //if v != current value
    pub fn wait(&self, _v: u32) {
        todo!();
    }

    pub fn wait_timeout(&self, _v: u32, _timeout: Duration) -> bool {
        todo!();
    }
    pub fn wake_single(&self) {
        todo!();
    }
    pub fn wake_all(&self) {
        todo!();
    }
}
/*
// SUPER HANDWAVEY
// YOU WILL NEED LOCKING THAT I DIDNT WRITE OUT (you == zuurr#9735)

// all the red is by design
pub fn futex_wait(atom: &AtomicU32, value: usize, current_thread: ThreadID) {
    let address = atomic as *const _ as usize;
    let waiters = waiters_for(address); // Hold lock
    waiters.add(current_thread);
    if self.load() == value {
        current_thread.sleep();
    } else {
        waiters.remove(current_thread);
    }
}

pub fn futex_wake(atom: &AtomicU32, threads_to_wake: usize) {
    let address = atomic as *const _ as usize;
    let waiters = waiters_for(address);
    for waiting_thread in waiters.into_iter().take(threads_to_wake) {
        waiting_thread.wake()
    }
}
*/

struct FutexWaitlist {
    address: u8,
    // data: Vec<ThreadID>,
}

impl FutexWaitlist {
    pub fn remove(&mut self) {
        todo!();
    }
    pub fn add(&mut self) {
        todo!();
    }
}
