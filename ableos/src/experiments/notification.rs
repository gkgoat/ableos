pub type thumbnail = u8;

pub struct Notification {
    thumbnail: thumbnail,
    text_body: String,
    time: u64,
}
