use alloc::{string::String, vec, vec::Vec};

pub static CLIPBOARD: spin::Mutex<Clipboard> = spin::Mutex::new(Clipboard::new());

#[derive(Debug)]
pub enum Mime {
    None,
    Text(String),
}

// ctrl+v paste but not pop and pastes
// ctrl+shift+v pops from the stack and pastes
// ctrl+c pushes to the stack
// ctrl+shift+< move stack pointer left
// ctrl+shift+> move stack pointer right

pub struct Clipboard {
    pub index: usize,
    pub pages: Vec<Mime>,
}

impl Clipboard {
    pub const fn new() -> Clipboard {
        Clipboard {
            index: 0,
            pages: vec![],
        }
    }

    pub fn clear(&mut self) {
        self.pages = vec![];
    }

    pub fn set_index(&mut self, index_new: usize) {
        self.index = index_new;
    }

    pub fn clip_end(&mut self) {
        self.index = 0;
    }

    pub fn clip_home(&mut self) {
        self.index = self.pages.len();
    }

    pub fn copy(&mut self, copy_mime: Mime) {
        self.pages.push(copy_mime);
    }

    pub fn paste(&mut self) -> &Mime {
        &self.pages[self.index] as _
    }
}
