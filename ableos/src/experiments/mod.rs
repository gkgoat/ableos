#![allow(dead_code)]

pub mod absi;
pub mod clip;
pub mod futex;
pub mod kinfo;
pub mod mail;
pub mod server;
pub mod systeminfo;
pub mod virtual_memory;
pub mod y_compositor;

pub const BANNER: &str = include_str!("banner.txt");
