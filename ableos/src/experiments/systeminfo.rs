// Can be standardized
// NOTE: move the file to the src/ dir

pub const KERNEL_VERSION: &str = env!("CARGO_PKG_VERSION");
#[cfg(debug_assertions)]
/// A constant to check if the kernel is in debug mode
pub const RELEASE_TYPE: &str = "debug";
#[cfg(not(debug_assertions))]
/// A constant to check if the kernel is in release mode
pub const RELEASE_TYPE: &str = "release";

pub struct SystemMemory {
    pub used: usize,
    pub total: usize,
}

impl core::fmt::Display for SystemMemory {
    fn fmt(&self, f: &mut core::fmt::Formatter) -> core::fmt::Result {
        write!(f, "{} Bytes / {} Bytes", self.used, self.total)
    }
}
