pub struct Compositor;
impl Compositor {
    pub fn new() -> Self {
        Self
    }
}

impl Default for Compositor {
    fn default() -> Self {
        Self::new()
    }
}
