// use crate::driver_traits::graphics::Point;

pub type MenuBar = Vec<MenuOption>;

pub struct MenuOption {
    symbol: char,
}

pub struct Window {
    title: String,
    // position: Point,
    fullscreen: bool,
}

// all of these should return a result
impl Window {
    pub fn new(title: String, /*position: Point,*/ fullscreen: bool) -> Self {
        Self {
            title,
            // position,
            fullscreen,
        }
    }

    pub fn fullscreen(&mut self) {
        self.fullscreen = true;
    }
    pub fn revert_fullscreen(&mut self) {
        self.fullscreen = false;
    }
    pub fn set_title(&mut self) {
        todo!();
    }
    pub fn set_position(&mut self /*pos: Point*/) {
        // self.position = pos;
    }
}
