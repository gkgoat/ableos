pub mod aalloc;
pub use aalloc::*;

pub const HEAP_START: usize = 0x_4444_4444_0000;
pub const HEAP_MULTIPLIER: usize = 100000;
pub const HEAP_BASE: usize = 100;
pub const HEAP_SIZE: usize = HEAP_BASE * HEAP_MULTIPLIER;

/*
#[global_allocator]
pub static ALLOCATOR: LockedHeap = LockedHeap::empty();

#[alloc_error_handler]
fn alloc_error_handler(layout: alloc::alloc::Layout) -> ! {
    panic!("allocation error: {:?}", layout)
}


*/
