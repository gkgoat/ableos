use core::fmt::{Arguments, Error};

pub struct Serialout;
impl Serialout {
    pub fn write_fmt(&mut self, arg: Arguments<'_>) /*-> Result<(), Error> */
    {
        let _ = core::fmt::Write::write_fmt(self, arg);
        //   Ok(())
    }
}

impl core::fmt::Write for Serialout {
    #[cfg(target_arch = "aarch64")]
    fn write_str(&mut self, s: &str) -> Result<(), Error> {
        // Don't actually print anything yet lmao
        Ok(())
    }
    #[cfg(target_arch = "x86_64")]
    fn write_str(&mut self, s: &str) -> Result<(), Error> {
        crate::arch::drivers::serial::_print(format_args!("{}", s));

        Ok(())
    }
    #[cfg(target_arch = "riscv64")]
    fn write_str(&mut self, s: &str) -> Result<(), Error> {
        write!(crate::arch::drivers::uart::Uart::new(0x1000_0000), "{}", s)
    }
    fn write_char(&mut self, c: char) -> core::fmt::Result {
        self.write_str(c.encode_utf8(&mut [0; 4]))
    }
    fn write_fmt(mut self: &mut Self, args: Arguments<'_>) -> core::fmt::Result {
        core::fmt::write(&mut self, args)
    }
}

#[macro_export]
macro_rules! serial_print {
    () => {
        ::core::write!($crate::serial_print::Serialout, "")
    };
    ($($tt:tt)*) => {
        ::core::write!($crate::serial_print::Serialout, $($tt)*)
    };
}

#[macro_export]
macro_rules! serial_println {
    ($($tt:tt)*) => {
        ::core::writeln!($crate::serial_print::Serialout, $($tt)*)
    };
}
