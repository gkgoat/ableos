/// # clparse
/// simple command line parser for ableOS

#[derive(Debug, Clone)]
pub struct Argument {
    key: String,
    value: String,
}

#[derive(Debug, Clone)]
pub struct Command {
    pub root_command: String,
    // arguments: HashMap<String, String>,
    pub arguments: Vec<Argument>,
}

impl Command {
    pub fn parse(command: String) -> Command {
        let split_command = command.split('?');
        let mut root = "".to_string();
        let mut args: Vec<Argument> = vec![];
        for (root_count, subcommand) in split_command.enumerate() {
            match root_count {
                0 => root = subcommand.to_string(),
                1 => {
                    for subarg in subcommand.split('&') {
                        let mut arg1 = "";
                        let mut arg2 = "";
                        for (n, arg) in subarg.split('=').enumerate() {
                            if n == 0 {
                                arg1 = arg;
                            } else {
                                arg2 = arg;
                            }
                        }
                        let arg_struct = Argument {
                            key: arg1.to_string(),
                            value: arg2.to_string(),
                        };
                        args.push(arg_struct);
                    }
                }
                _ => {}
            }
        }

        Command {
            root_command: root,
            arguments: args,
        }
    }
}

pub fn test() {
    let x = Command::parse("hi?there=uwu&hi=abc".to_string());

    let y = &x.arguments[0];

    trace!("{}", x.root_command);
    trace!("{:?}", y);
    trace!("{}", y.key);
    trace!("{}", y.value);
}
