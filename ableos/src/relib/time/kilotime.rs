use super::Time;
use core::fmt::{Display, Error, Formatter};

#[derive(Debug, Clone, Copy)]
#[repr(transparent)]
pub struct Kilosecond(u32);
impl Kilosecond {
    pub fn from_ms(ms: u32) -> Self {
        Self(ms)
    }

    pub fn from_sec(sec: u32) -> Self {
        Self(sec * 1000)
    }

    pub fn from_minutes(min: u32) -> Self {
        Self(min * 60 * 1000)
    }

    pub fn from_hours(hrs: u32) -> Self {
        Self(hrs * 60 * 60 * 1000)
    }

    pub fn from_days(days: u32) -> Self {
        Self(days * 24 * 60 * 60 * 1000)
    }

    pub fn s(&self) -> u32 {
        (self.0 % 1000)
    }

    pub fn k(&self) -> u32 {
        self.0 / 1000
    }
}

impl Display for Kilosecond {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        write![f, "{}K {}S", self.k(), self.s()]
    }
}

impl core::ops::Add for Kilosecond {
    type Output = Self;
    fn add(self, rhs: Self) -> Self {
        Self(self.0 + rhs.0)
    }
}

impl core::ops::Sub for Kilosecond {
    type Output = Self;
    fn sub(self, rhs: Self) -> Self {
        Self(self.0 - rhs.0)
    }
}

impl From<Time> for Kilosecond {
    fn from(t: Time) -> Self {
        Self((t.hour as u32 * 3600 + t.minutes as u32 * 60 + t.seconds as u32) * 1000)
    }
}
