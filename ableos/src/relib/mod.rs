//! A simple reimplimentation of the the rust standard library

pub mod clparse;
pub mod encoding;
pub mod image;
pub mod math;
pub mod network;
pub mod time;

pub struct VectorTwo {
    pub x: i32,
    pub y: i32,
}
