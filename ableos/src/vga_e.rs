use vga::{colors::Color16, writers::Graphics640x480x16};

pub static VGAE_BUFF_OFFSET_X: spin::Mutex<u8> = spin::Mutex::new(0);
pub static VGAE_BUFF_OFFSET_Y: spin::Mutex<u8> = spin::Mutex::new(0);
pub static VGAE: spin::Mutex<Graphics640x480x16> = {
    let xyz = Graphics640x480x16::new();
    // xyz.set_mode();
    spin::Mutex::new(xyz)
};

/// Converts a number to ... i forgor 💀
pub const fn num_to_vga16(num: u8) -> Color16 {
    use Color16::*;

    match num {
        0 => Black,
        1 => Blue,
        2 => Green,
        3 => Cyan,
        4 => Red,
        5 => Magenta,
        6 => Brown,
        7 => LightGrey,
        8 => DarkGrey,
        9 => LightBlue,
        10 => LightGreen,
        11 => LightCyan,
        12 => LightRed,
        13 => Pink,
        14 => Yellow,
        15 => White,
        // NOTE: Leasve the in
        _ => Color16::Pink,
    }
}
