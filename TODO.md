# AbleOS
## General
- [ ] Improve EXT2
- [ ] Remove x86 specific code and refine the boot process

## Capabilities
A new process should not have any capabilities at all until it is given them or requests them and is approved.
- [ ] Filesystem cap
    - [ ] Create a new filesystem
    - [ ] Unmount/Mount a filesystem
    - [ ] read a file
    - [ ] write a file
    - [ ] delete a file

- [ ] Network cap
    - [ ] open/close socket
    - [ ] bind/unbind socket

- [ ] Manage Process cap
    - [ ] spawn Process cap
    - [ ] kill Process cap

## Riscv

## ARM
- [ ] Get arm-version booting on real hardware

## Drivers
- [ ] Slim down driver specific program code
    - [ ] Remove entry/exit functions for drivers

## Filesystem
- [ ] Create a vfs that ties into the capability system
- [ ] Remote home directory
    - [ ] local file caching
    - [ ] remote file changes
        - [ ] Update file if the remote file changes





# Tooling
## Repbuild
- [ ] make generation of the ext2 image possible

