//! Time

/// An internal structure that is used to keep track of the time
pub struct Time {
    /// The number of seconds since the kernel was started
    pub seconds: u64,

    /// The number of nanoseconds since the kernel was started
    pub nanoseconds: u32,
}
