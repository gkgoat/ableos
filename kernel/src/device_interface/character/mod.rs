//!

/// Character device interface.
pub trait CharacterDevice {
    /// Returns true if the device can be read from.
    fn can_read(&self) -> bool;

    /// Returns true if the device can be written to
    fn can_write(&self) -> bool;

    /// Reads a single character from the device
    fn read_char(&mut self) -> Option<char>;

    /// Writes a single character to the device and returns true if the write was successful
    fn write_char(&mut self, c: char) -> bool;

    /// Reset the device to its initial state
    fn reset(&mut self);

    /// initializes the device, returns true if successful
    fn initialize(&mut self) -> bool;
}
