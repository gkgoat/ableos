//! The ableOS kernel.

#![feature(alloc_error_handler)]
#![feature(arbitrary_enum_discriminant)]
#![feature(prelude_import)]
#![no_std]
#![deny(missing_docs)]

extern crate alloc;

pub mod aalloc;
pub mod allocator;
pub mod arch;
pub mod device_interface;
pub mod messaging;
// pub mod panic;
pub mod proccess;
pub mod syscalls;
pub mod task;
pub mod time;

use core::arch::asm;
use versioning::Version;

/// The number of ticks since the first CPU was started
// pub static TICK: AtomicU64 = AtomicU64::new(0);

/// Kernel's version
pub const KERNEL_VERSION: Version = Version {
    major: 0,
    minor: 1,
    patch: 2,
};

/*
/// called by arch specific timers to tick up all kernel related functions
pub fn tick() {
    let mut data = TICK.load(Relaxed);
    data = data.wrapping_add(1);

    TICK.store(data, Relaxed)
}
*/
/// Cause a software interrupt
pub fn software_int() {
    unsafe { asm!("int 54") }
}
