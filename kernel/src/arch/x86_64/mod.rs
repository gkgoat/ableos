///

pub fn sloop() {
    loop {
        unsafe {
            core::arch::asm!("hlt");
        }
    }
}
