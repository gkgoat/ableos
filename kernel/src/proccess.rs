//! Platform agnostic process

/// A process ID
pub type PID = u64;

/// Signals that can be sent to a process
#[repr(C)]
pub enum Signals {
    /// Terminate the process
    Terminate,

    /// Shutdown the process and allow it to shutdown cleanly
    Quit,
}
