//!

use crate::proccess::{Signals, PID};

/// All possible system calls
pub enum Syscall {
    /// Create a new process and return its PID
    CreateProcess,

    /// Send a signal to a process
    SendSignal(PID, Signals),

    /// Get the current process ID
    GetPID,

    /// Get the current time
    GetTime,

    /// Set the time
    SetTime,
    // ListInodes,
    // CreateInode,
    // RemoveInode,
    // OpenInode,
    // CloseInode,
}
