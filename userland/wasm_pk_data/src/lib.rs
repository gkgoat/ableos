#![no_std]

extern crate alloc;

use {
    alloc::{string::String, vec::Vec},
    serde::Deserialize,
};

#[derive(Debug, Deserialize)]
pub struct Version {
    pub major: u16,
    pub minor: u8,
    pub patch: u8,
}

#[derive(Debug, Deserialize)]
pub struct Metadata {
    pub name: String,
    pub version: Version,
    pub authors: Vec<String>,
}
