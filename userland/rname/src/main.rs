//! An implementation of the uname command.

use crate::Arch::*;
use core::fmt;

// an example string "Darwin Roadrunner.local 10.3.0 Darwin Kernel Version 10.3.0: Fri Feb 26 11:58:09 PST 2010; root:xnu-1504.3.12~1/RELEASE_I386 i386"
pub struct RName {
    pub arch: Arch,
}

#[derive(Debug, Clone, Copy)]
pub enum Arch {
    X86,
    X86_64,
    ARM,
    ARM64,
    PPC,
    PPC64,
    MIPS,
    MIPS64,
    SPARC,
    SPARC64,
    Unknown,
}

impl fmt::Display for Arch {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

fn main() {
    let mut rname_string = "".to_string();

    rname_string.push_str("ableOS");

    let arch = Some(X86_64);
    if let Some(arch) = arch {
        let fmt_str = format!(" {:?}", arch);
        rname_string.push_str(&fmt_str);
    }

    println!("{}", rname_string);
}
