enum FSReturns {
    /// The system call was successful
    Ok,

    /// The directory can not be created
    DirectoryCouldNotBeCreated,

    /// The directory could not be removed
    DirectoryCouldNotBeRemoved,

    ///
    FileCouldNotBeCreated,

    ///
    FileCouldNotBeRemoved,
    /// The file could not be opened

    FileCouldNotBeOpened,
    ///
    FileCouldNotBeClosed,
}; 

int create_directory(path) {
    return DirectoryCouldNotBeCreated;
}

///
int remove_directory(path) {
    return DirectoryCouldNotBeRemoved;
}
