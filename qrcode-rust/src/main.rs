use qrcode::{render::unicode, QrCode};

fn main() {
    let code = QrCode::new("https://www.youtube.com/watch?v=p7YXXieghto").unwrap();
    let image = code
        .render::<unicode::Dense1x2>()
        .dark_color(unicode::Dense1x2::Light)
        .light_color(unicode::Dense1x2::Dark)
        .build();
    println!("{}", image);
}
