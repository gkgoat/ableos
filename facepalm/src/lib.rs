#![no_std]

use log::*;

pub const VERSION: &str = env!("CARGO_PKG_VERSION");

#[cfg(debug_assertions)]
pub const RELEASE_TYPE: &str = "debug";

#[cfg(not(debug_assertions))]
pub const RELEASE_TYPE: &str = "release";

pub fn start_facepalm() {
    info!("facepalm 🤦 launched!");
    info!("facepalm 🤦 version: {}", VERSION);
    info!("facepalm 🤦 {} mode", RELEASE_TYPE);

    // Drop into a debug shell
}
