use logos::{Lexer, Logos};

#[derive(Logos, Debug, Clone, Copy, PartialEq)]
enum Token {
    #[regex(r"[ \t\n\f]+", logos::skip)]
    #[error]
    Error,

    #[regex("[1-9]+", num_parser)]
    Num(isize),
}
fn num_parser(lex: &mut Lexer<Token>) -> isize {
    let slice = lex.slice();
    let num_str: String = slice.into();
    let num = num_str.parse::<isize>();
    num.unwrap()
}

#[test]
pub fn num_test() {
    let mut lex = Token::lexer("5 42 75");
    assert_eq!(lex.next(), Some(Token::Num(5)));
    assert_eq!(lex.next(), Some(Token::Num(42)));
    assert_eq!(lex.next(), Some(Token::Num(75)));
}

#[test]
pub fn asl_simple_test() {
    let lex = Token::lexer(include_str!("../assets/asl/asl_simple.asl"));

    for token in lex {
        // println!("{:?}", token);
        assert_ne!(Token::Error, token);
    }
}
